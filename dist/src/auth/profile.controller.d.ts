import { SuccessStatus } from '../common';
import { AuthService } from './auth.service';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './resources/profile.resource';
export declare class ProfileController {
    private readonly service;
    constructor(service: AuthService);
    getProfile(): Promise<Profile>;
    updateProfile(request: UpdateProfileDto): Promise<SuccessStatus>;
}
