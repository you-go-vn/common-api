"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const api_item_response_1 = require("../common/swagger/api.item.response");
const auth_service_1 = require("./auth.service");
const profile_resource_1 = require("./resources/profile.resource");
const logout_dto_1 = require("./dto/logout.dto");
const jwt_auth_guard_1 = require("./stategy/jwt-auth.guard");
const common_2 = require("../common");
let AuthController = class AuthController {
    constructor(service) {
        this.service = service;
    }
    async logout() {
        return await this.service.logout();
    }
    async logoutAll(body) {
        return await this.service.logoutAll(body);
    }
};
__decorate([
    (0, swagger_1.ApiOperation)({ summary: 'Đăng xuất phiên hiện tại' }),
    (0, swagger_1.ApiConsumes)('application/x-www-form-urlencoded', 'application/json'),
    (0, api_item_response_1.ApiItemResponse)(common_2.SuccessStatus),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('logout'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logout", null);
__decorate([
    (0, swagger_1.ApiOperation)({
        summary: 'Đăng xuất tất cả - Có thể loại trừ thiết bị hiện tại',
    }),
    (0, swagger_1.ApiConsumes)('application/json'),
    (0, api_item_response_1.ApiItemResponse)(common_2.SuccessStatus),
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Post)('logout/all'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [logout_dto_1.LogoutDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logoutAll", null);
AuthController = __decorate([
    (0, swagger_1.ApiTags)('Auth'),
    (0, swagger_1.ApiExtraModels)(profile_resource_1.Profile),
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map