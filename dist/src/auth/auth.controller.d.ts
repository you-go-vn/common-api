import { AuthService } from './auth.service';
import { LogoutDto } from './dto/logout.dto';
import { SuccessStatus } from '../common';
export declare class AuthController {
    private readonly service;
    constructor(service: AuthService);
    logout(): Promise<SuccessStatus>;
    logoutAll(body: LogoutDto): Promise<SuccessStatus>;
}
