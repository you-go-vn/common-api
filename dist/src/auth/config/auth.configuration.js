"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authConfiguration = void 0;
const authConfiguration = () => ({
    jwt: {
        secret: process.env.JWT_SECRET || '',
        signOptions: {
            expiresIn: process.env.JWT_EXPIRES_IN || '10days',
        },
    },
    auth_microservice: {
        host: process.env.CLIENT_AUTH_HOST || '127.0.0.1',
        port: parseInt(process.env.CLIENT_AUTH_PORT, 10),
    },
});
exports.authConfiguration = authConfiguration;
//# sourceMappingURL=auth.configuration.js.map