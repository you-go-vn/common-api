export declare const authConfiguration: () => {
    jwt: {
        secret: string;
        signOptions: {
            expiresIn: string;
        };
    };
    auth_microservice: {
        host: string;
        port: number;
    };
};
