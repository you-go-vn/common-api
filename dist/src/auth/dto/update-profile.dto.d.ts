export declare class UpdateProfileDto {
    fullname: string;
    email: string;
    avatar: string;
}
export declare class UpdateProfileClientDto extends UpdateProfileDto {
    id: number;
}
