export declare class EmailResetPasswordDto {
    email: string;
}
export declare class TokenResetPasswordDto {
    token: string;
}
declare const ResetPasswordDto_base: import("@nestjs/common").Type<Omit<TokenResetPasswordDto, never>>;
export declare class ResetPasswordDto extends ResetPasswordDto_base {
    account: string;
    password: string;
    password_confirmation: string;
}
export {};
