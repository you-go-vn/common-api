"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResetPasswordDto = exports.TokenResetPasswordDto = exports.EmailResetPasswordDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const validators_1 = require("../../common/validators");
class EmailResetPasswordDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsEmail)(),
    __metadata("design:type", String)
], EmailResetPasswordDto.prototype, "email", void 0);
exports.EmailResetPasswordDto = EmailResetPasswordDto;
class TokenResetPasswordDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        description: 'được kèm trong link tới email hoặc trong kết quả trả về từ yêu cầu khôi phục mật khẩu qua số điện thoại',
    }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], TokenResetPasswordDto.prototype, "token", void 0);
exports.TokenResetPasswordDto = TokenResetPasswordDto;
class ResetPasswordDto extends (0, swagger_1.OmitType)(TokenResetPasswordDto, []) {
}
__decorate([
    (0, swagger_1.ApiProperty)({ description: 'email hoặc số điện thoại gửi yêu cầu' }),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], ResetPasswordDto.prototype, "account", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.MinLength)(6),
    __metadata("design:type", String)
], ResetPasswordDto.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, validators_1.Match)('password'),
    __metadata("design:type", String)
], ResetPasswordDto.prototype, "password_confirmation", void 0);
exports.ResetPasswordDto = ResetPasswordDto;
//# sourceMappingURL=reset-password.dto.js.map