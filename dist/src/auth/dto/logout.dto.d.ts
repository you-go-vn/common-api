export declare class LogoutDto {
    includeCurrentSession: boolean;
}
export declare class LogoutClientDto {
    token: string;
    userId: number;
}
declare const LogoutAllClientDto_base: import("@nestjs/common").Type<LogoutClientDto & LogoutDto>;
export declare class LogoutAllClientDto extends LogoutAllClientDto_base {
}
export {};
