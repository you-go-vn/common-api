import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { Request } from 'express';
import { LoginAccountDto } from '.';
import { SuccessStatus } from '../common';
import { LogoutDto } from './dto/logout.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { AuthUser } from './resources/auth-user.resouce';
import { Profile } from './resources/profile.resource';
export declare class AuthService {
    protected readonly client: ClientProxy;
    private request;
    private readonly jwtService;
    readonly cmd: string;
    constructor(client: ClientProxy, request: Request, jwtService: JwtService);
    login(body: LoginAccountDto): Promise<Profile>;
    findByToken(): Promise<Profile>;
    logout(): Promise<SuccessStatus>;
    logoutAll(body: LogoutDto): Promise<SuccessStatus>;
    getProfile(): Promise<Profile>;
    updateProfile(body: UpdateProfileDto): Promise<SuccessStatus>;
    generateToken(auth: AuthUser): Promise<Profile>;
    token(): string;
    userId(): number;
    addToken(token: string, userId: number): Promise<boolean>;
}
