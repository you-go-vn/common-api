"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const jwt_1 = require("@nestjs/jwt");
const microservices_1 = require("@nestjs/microservices");
const class_transformer_1 = require("class-transformer");
const rxjs_1 = require("rxjs");
const common_2 = require("../common");
const profile_resource_1 = require("./resources/profile.resource");
let AuthService = class AuthService {
    constructor(client, request, jwtService) {
        this.client = client;
        this.request = request;
        this.jwtService = jwtService;
        this.cmd = 'auth';
    }
    async login(body) {
        try {
            const pattern = { cmd: this.cmd, action: 'find_by_account_and_password' };
            const user = await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, body));
            return await this.generateToken(user);
        }
        catch (err) {
            throw new common_1.NotFoundException();
        }
    }
    async findByToken() {
        try {
            const pattern = { cmd: this.cmd, action: 'find_by_token' };
            const token = this.token();
            const authUser = await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, { token: token }));
            return this.generateToken(authUser);
        }
        catch (err) {
            throw new common_1.UnauthorizedException();
        }
    }
    async logout() {
        try {
            const user = await this.getProfile();
            const logout = {
                token: user.token,
                userId: user.id,
            };
            const pattern = { cmd: this.cmd, action: 'remove_token_to_user' };
            const logoutResult = await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, logout));
            return common_2.result.successStatus(logoutResult.affected === 1);
        }
        catch (err) {
            return common_2.result.successStatus(false);
        }
    }
    async logoutAll(body) {
        try {
            const logoutAll = Object.assign({ token: this.token(), userId: this.userId() }, body);
            const pattern = { cmd: this.cmd, action: 'remove_all_token_to_user' };
            await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, logoutAll));
            return common_2.result.successStatus(true);
        }
        catch (err) {
            return common_2.result.successStatus(true);
        }
    }
    async getProfile() {
        const authUser = this.request.user;
        return await this.generateToken(authUser);
    }
    async updateProfile(body) {
        try {
            const pattern = { cmd: this.cmd, action: 'update_profile' };
            const profile = await this.getProfile();
            const data = Object.assign({ id: profile.id }, body);
            const updateResult = await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, data));
            return common_2.result.successStatus(updateResult.affected === 1);
        }
        catch (err) {
            return common_2.result.successStatus(false);
        }
    }
    async generateToken(auth) {
        let token = this.token();
        if (!token) {
            const payload = {
                id: auth.id,
                email: auth.email,
            };
            token = this.jwtService.sign(payload);
            await this.addToken(token, auth.id);
        }
        return (0, class_transformer_1.plainToInstance)(profile_resource_1.Profile, Object.assign({ token: token }, auth));
    }
    token() {
        const { headers } = this.request;
        let token;
        if (headers.authorization)
            token = headers.authorization.split(' ')[1] || null;
        return token;
    }
    userId() {
        const { user } = this.request;
        return user.id;
    }
    async addToken(token, userId) {
        try {
            const pattern = { cmd: this.cmd, action: 'add_token' };
            return await (0, rxjs_1.lastValueFrom)(this.client.send(pattern, { token, userId }));
        }
        catch (err) {
            return null;
        }
    }
};
AuthService = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('AUTH_MICROSERVICE')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [microservices_1.ClientProxy, Object, jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map