import { ConfigService } from '@nestjs/config';
import { Strategy } from 'passport-jwt';
import { PayloadAuth } from './payload';
declare const JwtStrategy_base: new (...args: any[]) => Strategy;
export declare class JwtStrategy extends JwtStrategy_base {
    constructor(configService: ConfigService);
    validate({ id, email }: PayloadAuth): Promise<{
        id: number;
        email: string;
    }>;
}
export {};
