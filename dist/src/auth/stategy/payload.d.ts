export interface PayloadAuth {
    id: number;
    email: string;
}
