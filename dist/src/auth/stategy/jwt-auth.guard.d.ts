import { ExecutionContext } from '@nestjs/common';
import { AuthService } from '../auth.service';
declare const JwtAuthGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export declare class JwtAuthGuard extends JwtAuthGuard_base {
    private authService;
    constructor(authService: AuthService);
    canActivate(context: ExecutionContext): Promise<any>;
    handleRequest(err: any, user: any, info: any, context: any): any;
}
export {};
