import { ConfigService } from '@nestjs/config';
import { ClientsModuleOptionsFactory, TcpClientOptions } from '@nestjs/microservices';
export declare class ClientAuthConfig implements ClientsModuleOptionsFactory {
    private configService;
    constructor(configService: ConfigService);
    createClientOptions(): TcpClientOptions | Promise<TcpClientOptions>;
}
