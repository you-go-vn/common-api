"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiPaginateResponse = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const ApiPaginateResponse = (model, options) => {
    return (0, common_1.applyDecorators)((0, swagger_1.ApiOkResponse)({
        schema: {
            allOf: [
                {
                    properties: {
                        data: {
                            type: 'object',
                            properties: {
                                items: {
                                    type: 'array',
                                    items: { $ref: (0, swagger_1.getSchemaPath)(model) },
                                },
                                total: {
                                    type: 'number',
                                },
                                totalPage: {
                                    type: 'number',
                                },
                            },
                        },
                        code: { type: 'number', default: 200 },
                        message: { type: 'string' },
                    },
                },
            ],
        },
        status: (options === null || options === void 0 ? void 0 : options.status) || 200,
        description: (options === null || options === void 0 ? void 0 : options.description) || null,
    }));
};
exports.ApiPaginateResponse = ApiPaginateResponse;
//# sourceMappingURL=api.paginate.response.js.map