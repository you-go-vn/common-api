import { Type } from '@nestjs/common';
import { ApiResponseCustomOptions } from './api.response.options';
export declare const ApiListResponse: <TModel extends Type<any>>(model: TModel, options?: ApiResponseCustomOptions) => <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
