import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class ValidationFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): void;
}
