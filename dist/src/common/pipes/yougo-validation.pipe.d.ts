import { ArgumentMetadata, PipeTransform, ValidationError, ValidationPipe, BadRequestException } from '@nestjs/common';
export declare class YouGoValidationPipe extends ValidationPipe implements PipeTransform<any> {
    transform(value: any, metadata: ArgumentMetadata): Promise<any>;
    exceptionFactory: (errors: ValidationError[]) => ValidationException;
}
export interface Errors {
    field: string;
    errors: string[];
}
export declare class ValidationException extends BadRequestException {
    validationErrors: Errors[];
    constructor(validationErrors: Errors[]);
}
