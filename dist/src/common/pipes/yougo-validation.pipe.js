"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationException = exports.YouGoValidationPipe = void 0;
const common_1 = require("@nestjs/common");
const omitEmpty = require("omit-empty");
class YouGoValidationPipe extends common_1.ValidationPipe {
    constructor() {
        super(...arguments);
        this.exceptionFactory = (errors) => {
            const message = errors.map((error) => {
                return {
                    field: error.property,
                    errors: Object.values(error.constraints),
                };
            });
            return new ValidationException(message);
        };
    }
    async transform(value, metadata) {
        value = omitEmpty(value);
        return super.transform(value, metadata);
    }
}
exports.YouGoValidationPipe = YouGoValidationPipe;
class ValidationException extends common_1.BadRequestException {
    constructor(validationErrors) {
        super();
        this.validationErrors = validationErrors;
    }
}
exports.ValidationException = ValidationException;
//# sourceMappingURL=yougo-validation.pipe.js.map