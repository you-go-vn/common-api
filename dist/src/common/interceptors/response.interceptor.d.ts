import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
export interface DataResponse {
    data: any;
}
export interface Response {
    code: number;
    message: string;
    data: any;
}
export declare class ResponseInterceptor implements NestInterceptor<Response> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any>;
}
