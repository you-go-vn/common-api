import { ValidationArguments, ValidationOptions, ValidatorConstraintInterface } from 'class-validator';
export declare function VnPhoneNumber(validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
export declare class VnPhoneNumberConstraint implements ValidatorConstraintInterface {
    private pattern;
    validate(value: any): boolean;
    defaultMessage(args: ValidationArguments): any;
}
