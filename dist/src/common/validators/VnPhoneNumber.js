"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VnPhoneNumberConstraint = exports.VnPhoneNumber = void 0;
const class_validator_1 = require("class-validator");
function VnPhoneNumber(validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [validationOptions],
            validator: VnPhoneNumberConstraint,
        });
    };
}
exports.VnPhoneNumber = VnPhoneNumber;
let VnPhoneNumberConstraint = class VnPhoneNumberConstraint {
    constructor() {
        this.pattern = /(086|096|097|098|032|033|034|035|036|037|038|039|089|090|093|070|079|077|076|078|088|091|094|083|084|085|081|082|092|056|058|099|059)\d{7}$/;
    }
    validate(value) {
        return (0, class_validator_1.matches)(value, this.pattern);
    }
    defaultMessage(args) {
        const [constraints] = args.constraints;
        return (constraints === null || constraints === void 0 ? void 0 : constraints.message) || `Số điện thoại không hợp lệ`;
    }
};
VnPhoneNumberConstraint = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'VnPhoneNumber' })
], VnPhoneNumberConstraint);
exports.VnPhoneNumberConstraint = VnPhoneNumberConstraint;
//# sourceMappingURL=VnPhoneNumber.js.map