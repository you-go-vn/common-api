"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.result = exports.ValidationErrors = exports.Error = exports.ServerError = exports.SuccessStatus = exports.DeleteResult = exports.UpdateResult = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
class UpdateResult {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], UpdateResult.prototype, "raw", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Number)
], UpdateResult.prototype, "affected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Array)
], UpdateResult.prototype, "generatedMaps", void 0);
exports.UpdateResult = UpdateResult;
class DeleteResult {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Object)
], DeleteResult.prototype, "raw", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Number)
], DeleteResult.prototype, "affected", void 0);
exports.DeleteResult = DeleteResult;
class SuccessStatus {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Boolean)
], SuccessStatus.prototype, "success", void 0);
exports.SuccessStatus = SuccessStatus;
class ServerError {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], ServerError.prototype, "data", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ServerError.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ServerError.prototype, "message", void 0);
exports.ServerError = ServerError;
class Error {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], Error.prototype, "field", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], Error.prototype, "errors", void 0);
exports.Error = Error;
class ValidationErrors {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [Error] }),
    __metadata("design:type", Array)
], ValidationErrors.prototype, "errors", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ValidationErrors.prototype, "code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ValidationErrors.prototype, "message", void 0);
exports.ValidationErrors = ValidationErrors;
const successStatus = function (status) {
    return { success: status };
};
exports.result = { successStatus };
//# sourceMappingURL=result.resource.js.map