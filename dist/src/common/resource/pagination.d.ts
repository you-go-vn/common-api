export declare class Pagination<Entity> {
    items: Entity[];
    total: number;
    pageTotal: number;
}
export interface PaginationPagedInterface {
    page?: number;
    limit?: number;
}
export interface SearchQueryInterface {
    keywords?: string;
}
export declare class PaginationPaged implements PaginationPagedInterface {
    page?: number;
    limit?: number;
}
export declare class SearchQuery implements SearchQueryInterface {
    keywords?: string;
}
export declare class PaginationSearchQuery extends PaginationPaged implements SearchQueryInterface {
    keywords?: string;
}
export declare const paginate: {
    empty: () => Pagination<any>;
};
