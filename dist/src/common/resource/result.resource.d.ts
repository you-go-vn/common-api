import { Errors } from '../pipes/yougo-validation.pipe';
export interface ObjectLiteral {
    [key: string]: any;
}
export declare class UpdateResult {
    raw: any;
    affected?: number;
    generatedMaps: ObjectLiteral[];
}
export declare class DeleteResult {
    raw: any;
    affected?: number | null;
}
export declare class SuccessStatus {
    success: boolean;
}
export declare class ServerError {
    data: object;
    code: number;
    message: string;
}
export declare class Error implements Errors {
    field: string;
    errors: string[];
}
export declare class ValidationErrors {
    errors: Error[];
    code: number;
    message: string;
}
export declare const result: {
    successStatus: (status: boolean) => SuccessStatus;
};
