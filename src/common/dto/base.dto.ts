import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber } from 'class-validator';

export class UpdateId {
  @IsNumber()
  id: number;
}

export class SortingDto {
  @ApiProperty({ type: Number, isArray: true })
  @IsArray()
  ids: number[];
}
