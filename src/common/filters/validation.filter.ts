import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
@Catch(BadRequestException)
export class ValidationFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    response.json({
      errors: exception.validationErrors, // From GlobalValidationPipe.exceptionFactory
      code: HttpStatus.BAD_REQUEST,
      message: 'invalid',
    });
  }
}
