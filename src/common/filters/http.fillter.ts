import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
@Catch()
export class HttpFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const data = {};
    const message = exception.message;
    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    if (exception instanceof Array) {
      response.status(HttpStatus.BAD_REQUEST).json({
        errors: exception,
        code: HttpStatus.BAD_REQUEST,
        message: 'invalid',
      });
    } else {
      response.status(status).json({
        data: data,
        code: status,
        message: message,
      });
    }
  }
}
