import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform, Type } from 'class-transformer';

@Exclude()
export class BaseResource {
  @Expose({ toClassOnly: true })
  @Type(() => Number)
  id: number;

  @ApiProperty()
  @Expose()
  @Transform(({ obj }: { obj: BaseResource }) => obj.id)
  _id: number;
}
