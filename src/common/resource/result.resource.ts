import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { Errors } from '../pipes/yougo-validation.pipe';

export interface ObjectLiteral {
  [key: string]: any;
}

export class UpdateResult {
  @ApiProperty()
  @Expose()
  raw: any;

  @ApiProperty()
  @Expose()
  affected?: number;

  @ApiProperty()
  @Expose()
  generatedMaps: ObjectLiteral[];
}

export class DeleteResult {
  @ApiProperty()
  @Expose()
  raw: any;

  @ApiProperty()
  @Expose()
  affected?: number | null;
}
export class SuccessStatus {
  @ApiProperty()
  @Expose()
  success: boolean;
}

export class ServerError {
  @ApiProperty()
  data: object;

  @ApiProperty()
  code: number;

  @ApiProperty()
  message: string;
}

export class Error implements Errors {
  @ApiProperty()
  field: string;
  @ApiProperty()
  errors: string[];
}

export class ValidationErrors {
  @ApiProperty({ type: [Error] })
  errors: Error[];

  @ApiProperty()
  code: number;

  @ApiProperty()
  message: string;
}

const successStatus = function (status: boolean): SuccessStatus {
  return { success: status };
};
export const result = { successStatus };
