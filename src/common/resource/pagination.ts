import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class Pagination<Entity> {
  public items: Entity[];
  public total: number;
  public pageTotal: number;
}

export interface PaginationPagedInterface {
  page?: number;
  limit?: number;
}

export interface SearchQueryInterface {
  keywords?: string;
}

export class PaginationPaged implements PaginationPagedInterface {
  @ApiPropertyOptional()
  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  public page?: number = 0;

  @ApiPropertyOptional()
  @IsNumber({})
  @IsOptional()
  @Type(() => Number)
  public limit?: number = 10;
}

export class SearchQuery implements SearchQueryInterface {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  @Type(() => String)
  keywords?: string;
}
export class PaginationSearchQuery
  extends PaginationPaged
  implements SearchQueryInterface
{
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  @Type(() => String)
  keywords?: string;
}

const empty = function (): Pagination<any> {
  return { items: [], total: 0, pageTotal: 1 };
};
export const paginate = { empty };
