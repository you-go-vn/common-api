export interface ApiResponseCustomOptions {
  status?: number;
  description?: string;
}
