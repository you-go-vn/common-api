import { applyDecorators, Type } from '@nestjs/common';
import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { ApiResponseCustomOptions } from './api.response.options';

export const ApiPaginateResponse = <TModel extends Type<any>>(
  model: TModel,
  options?: ApiResponseCustomOptions,
) => {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        allOf: [
          {
            properties: {
              data: {
                type: 'object',
                properties: {
                  items: {
                    type: 'array',
                    items: { $ref: getSchemaPath(model) },
                  },
                  total: {
                    type: 'number',
                  },
                  totalPage: {
                    type: 'number',
                  },
                },
              },
              code: { type: 'number', default: 200 },
              message: { type: 'string' },
            },
          },
        ],
      },
      status: options?.status || 200,
      description: options?.description || null,
    }),
  );
};
