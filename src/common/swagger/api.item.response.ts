import { applyDecorators, Type } from '@nestjs/common';
import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { ApiResponseCustomOptions } from './api.response.options';

export const ApiItemResponse = <TModel extends Type<any>>(
  model: TModel,
  options?: ApiResponseCustomOptions,
) => {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        allOf: [
          {
            properties: {
              data: {
                type: 'object',
                $ref: getSchemaPath(model),
              },
              code: { type: 'number', default: 200 },
              message: { type: 'string' },
            },
          },
        ],
      },
      status: options?.status || 200,
      description: options?.description || null,
    }),
  );
};
