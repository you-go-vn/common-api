import {
  matches,
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

export function VnPhoneNumber(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [validationOptions],
      validator: VnPhoneNumberConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'VnPhoneNumber' })
export class VnPhoneNumberConstraint implements ValidatorConstraintInterface {
  private pattern =
    /(086|096|097|098|032|033|034|035|036|037|038|039|089|090|093|070|079|077|076|078|088|091|094|083|084|085|081|082|092|056|058|099|059)\d{7}$/;
  validate(value: any) {
    return matches(value, this.pattern);
  }
  defaultMessage(args: ValidationArguments) {
    const [constraints] = args.constraints;
    return constraints?.message || `Số điện thoại không hợp lệ`;
  }
}
