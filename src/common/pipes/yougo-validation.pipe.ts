import {
  ArgumentMetadata,
  PipeTransform,
  ValidationError,
  ValidationPipe,
  BadRequestException,
} from '@nestjs/common';
import * as omitEmpty from 'omit-empty';

export class YouGoValidationPipe
  extends ValidationPipe
  implements PipeTransform<any>
{
  async transform(value: any, metadata: ArgumentMetadata) {
    value = omitEmpty(value);
    return super.transform(value, metadata);
  }

  exceptionFactory = (errors: ValidationError[]) => {
    const message = errors.map((error) => {
      return {
        field: error.property,
        errors: Object.values(error.constraints),
      };
    });
    return new ValidationException(message);
  };
}

export interface Errors {
  field: string;
  errors: string[];
}

export class ValidationException extends BadRequestException {
  constructor(public validationErrors: Errors[]) {
    super();
  }
}
