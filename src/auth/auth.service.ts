import {
  Global,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { plainToInstance } from 'class-transformer';
import { Request } from 'express';
import { lastValueFrom } from 'rxjs';
import { LoginAccountDto } from '.';
import { DeleteResult, result, SuccessStatus, UpdateResult } from '../common';
import {
  LogoutAllClientDto,
  LogoutClientDto,
  LogoutDto,
} from './dto/logout.dto';
import {
  UpdateProfileClientDto,
  UpdateProfileDto,
} from './dto/update-profile.dto';
import { AuthUser } from './resources/auth-user.resouce';
import { Profile } from './resources/profile.resource';
import { PayloadAuth } from './stategy/payload';

@Global()
@Injectable()
export class AuthService {
  readonly cmd: string = 'auth';

  constructor(
    @Inject('AUTH_MICROSERVICE') protected readonly client: ClientProxy,
    @Inject(REQUEST) private request: Request,
    private readonly jwtService: JwtService,
  ) {}
  async login(body: LoginAccountDto): Promise<Profile> {
    try {
      const pattern = { cmd: this.cmd, action: 'find_by_account_and_password' };
      const user = await lastValueFrom(
        this.client.send<AuthUser>(pattern, body),
      );
      return await this.generateToken(user);
    } catch (err) {
      throw new NotFoundException();
    }
  }

  async findByToken(): Promise<Profile> {
    try {
      const pattern = { cmd: this.cmd, action: 'find_by_token' };
      const token = this.token();
      const authUser = await lastValueFrom(
        this.client.send<AuthUser>(pattern, { token: token }),
      );
      return this.generateToken(authUser);
    } catch (err) {
      throw new UnauthorizedException();
    }
  }
  async logout(): Promise<SuccessStatus> {
    try {
      const user: Profile = await this.getProfile();
      const logout: LogoutClientDto = {
        token: user.token,
        userId: user.id,
      };
      const pattern = { cmd: this.cmd, action: 'remove_token_to_user' };
      const logoutResult: DeleteResult = await lastValueFrom(
        this.client.send<DeleteResult>(pattern, logout),
      );
      return result.successStatus(logoutResult.affected === 1);
    } catch (err) {
      return result.successStatus(false);
    }
  }

  async logoutAll(body: LogoutDto): Promise<SuccessStatus> {
    try {
      const logoutAll: LogoutAllClientDto = {
        token: this.token(),
        userId: this.userId(),
        ...body,
      };
      const pattern = { cmd: this.cmd, action: 'remove_all_token_to_user' };
      await lastValueFrom(this.client.send<DeleteResult>(pattern, logoutAll));
      return result.successStatus(true);
    } catch (err) {
      return result.successStatus(true);
    }
  }

  async getProfile(): Promise<Profile> {
    const authUser: AuthUser = this.request.user;
    return await this.generateToken(authUser);
  }

  async updateProfile(body: UpdateProfileDto): Promise<SuccessStatus> {
    try {
      const pattern = { cmd: this.cmd, action: 'update_profile' };
      const profile: Profile = await this.getProfile();
      const data: UpdateProfileClientDto = {
        id: profile.id,
        ...body,
      };
      const updateResult: UpdateResult = await lastValueFrom(
        this.client.send<UpdateResult>(pattern, data),
      );
      return result.successStatus(updateResult.affected === 1);
    } catch (err) {
      return result.successStatus(false);
    }
  }

  async generateToken(auth: AuthUser): Promise<Profile> {
    let token: string = this.token();
    if (!token) {
      const payload: PayloadAuth = {
        id: auth.id,
        email: auth.email,
      };
      token = this.jwtService.sign(payload);
      await this.addToken(token, auth.id);
    }
    return plainToInstance(Profile, { token: token, ...auth });
  }

  token(): string {
    const { headers } = this.request;
    let token: string;
    if (headers.authorization)
      token = headers.authorization.split(' ')[1] || null;
    return token;
  }
  userId(): number {
    const { user } = this.request;
    return user.id;
  }

  async addToken(token: string, userId: number): Promise<boolean> {
    try {
      const pattern = { cmd: this.cmd, action: 'add_token' };
      return await lastValueFrom(
        this.client.send<boolean>(pattern, { token, userId }),
      );
    } catch (err) {
      return null;
    }
  }
}
