import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './stategy/jwt.strategy';
import { ClientsModule } from '@nestjs/microservices';
import { ProfileController } from './profile.controller';
import { ClientAuthConfig } from './stategy/client.auth.config';
import { JwtConfig } from './config/jwt.config';
import { authConfiguration } from './config/auth.configuration';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [authConfiguration],
    }),
    JwtModule.registerAsync({
      imports: [ConfigService],
      useClass: JwtConfig,
    }),
    ClientsModule.registerAsync([
      {
        name: 'AUTH_MICROSERVICE',
        useClass: ClientAuthConfig,
        inject: [ConfigService],
      },
    ]),
    PassportModule,
  ],
  controllers: [AuthController, ProfileController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService, ClientsModule],
})
export class AuthModule {}
