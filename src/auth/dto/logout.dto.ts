import { ApiPropertyOptional, IntersectionType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsOptional } from 'class-validator';

export class LogoutDto {
  @ApiPropertyOptional({
    description: 'Thoát cả phiên hiện tại hay không?',
    default: false,
  })
  @IsBoolean()
  @IsOptional()
  @Type(() => Boolean)
  includeCurrentSession: boolean;
}

export class LogoutClientDto {
  token: string;
  userId: number;
}
export class LogoutAllClientDto extends IntersectionType(
  LogoutClientDto,
  LogoutDto,
) {}
