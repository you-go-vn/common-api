import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class LoginAccountDto {
  @ApiProperty({ default: 'account1@yougo.vn' })
  @IsString()
  account: string;

  @ApiProperty({ default: 'pass!@#123' })
  @IsString()
  password: string;
}
