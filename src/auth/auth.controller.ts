import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiExtraModels,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ApiItemResponse } from '../common/swagger/api.item.response';
import { AuthService } from './auth.service';
import { Profile } from './resources/profile.resource';
import { LogoutDto } from './dto/logout.dto';
import { JwtAuthGuard } from './stategy/jwt-auth.guard';
import { SuccessStatus } from '../common';

@ApiTags('Auth')
@ApiExtraModels(Profile)
@Controller('auth')
export class AuthController {
  constructor(private readonly service: AuthService) {}
  @ApiOperation({ summary: 'Đăng xuất phiên hiện tại' })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @ApiItemResponse(SuccessStatus)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('logout')
  async logout(): Promise<SuccessStatus> {
    return await this.service.logout();
  }

  @ApiOperation({
    summary: 'Đăng xuất tất cả - Có thể loại trừ thiết bị hiện tại',
  })
  @ApiConsumes('application/json')
  @ApiItemResponse(SuccessStatus)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('logout/all')
  async logoutAll(@Body() body: LogoutDto): Promise<SuccessStatus> {
    return await this.service.logoutAll(body);
  }
}
