export {};
declare global {
  namespace Express {
    interface User {
      id: number;

      _id: number;

      fullname: string;

      phone_number: string;

      verified_phone: boolean;

      email: string;

      no_password: boolean;

      gender: number;

      avatar: string;

      availability: boolean;

      complete_profile: boolean;

      token: string;
    }
  }
}
