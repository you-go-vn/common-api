import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth.service';
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private authService: AuthService) {
    super();
  }
  async canActivate(context: ExecutionContext): Promise<any> {
    const request = context.switchToHttp().getRequest();
    request.user = await this.authService.findByToken();
    return await super.canActivate(context);
  }

  handleRequest(err: any, user: any, info: any, context): any {
    const request = context.switchToHttp().getRequest();
    return request.user;
  }
}
