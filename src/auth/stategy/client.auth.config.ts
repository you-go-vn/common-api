import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ClientsModuleOptionsFactory,
  TcpClientOptions,
  Transport,
} from '@nestjs/microservices';

@Injectable()
export class ClientAuthConfig implements ClientsModuleOptionsFactory {
  constructor(private configService: ConfigService) {}

  createClientOptions(): TcpClientOptions | Promise<TcpClientOptions> {
    return {
      transport: Transport.TCP,
      options: this.configService.get('auth_microservice'),
    };
  }
}
