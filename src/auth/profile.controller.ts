import { Body, Controller, Get, Put, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiExtraModels,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ApiItemResponse, SuccessStatus } from '../common';
import { AuthService } from './auth.service';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './resources/profile.resource';
import { JwtAuthGuard } from './stategy/jwt-auth.guard';

@ApiTags('Profile')
@ApiBearerAuth()
@ApiExtraModels(Profile)
@UseGuards(JwtAuthGuard)
@Controller('profile')
export class ProfileController {
  constructor(private readonly service: AuthService) {}

  @ApiOperation({ summary: 'Lấy thông tin tài khoản' })
  @ApiItemResponse(Profile)
  @Get()
  getProfile(): Promise<Profile> {
    return this.service.getProfile();
  }

  @ApiOperation({ summary: 'Cập nhật thông tin tài khoản' })
  @ApiConsumes('application/x-www-form-urlencoded', 'application/json')
  @ApiItemResponse(SuccessStatus)
  @Put()
  async updateProfile(
    @Body() request: UpdateProfileDto,
  ): Promise<SuccessStatus> {
    return await this.service.updateProfile(request);
  }
}
