import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { AuthUser } from './auth-user.resouce';
@Exclude()
export class Profile extends AuthUser {
  @ApiProperty()
  @Expose()
  token: string;
}
