import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class AuthUser {
  @ApiProperty()
  @Expose()
  id: number;

  @ApiProperty()
  @Expose()
  fullname: string;

  @ApiProperty()
  @Expose()
  phone_number: string;

  @ApiProperty()
  @Expose()
  verified_phone: boolean;

  @ApiProperty()
  @Expose()
  email: string;

  @ApiProperty()
  @Expose()
  no_password: boolean;

  @ApiProperty()
  @Expose()
  gender: number;

  @ApiProperty()
  @Expose()
  avatar: string;

  @ApiProperty()
  @Expose()
  availability: boolean;

  @ApiProperty()
  @Expose()
  complete_profile: boolean;
}
